# rdf_img

## Installation 

```
opam install rdf
make
```

## Usage

Creation of dot and svg from turtle file:
```
./rdf_img example.ttl
```
