let base_iri = Iri.of_string "http://foo.bar.net";;
let g = Rdf_graph.open_graph base_iri;;
let ttl_file = Sys.argv.(1);;

Rdf_ttl.from_file g ttl_file;;

let dot_file = (Filename.chop_extension ttl_file)^".dot";;
let svg_file = (Filename.chop_extension dot_file)^".svg";;
let dot_code = Rdf_dot.dot_of_graph g;;
let () =
  let oc = open_out dot_file in
  output_string oc dot_code;
  close_out oc;;
let com = Printf.sprintf "dot -Grankdir=TB -Tsvg -o %s %s"
  (Filename.quote svg_file) (Filename.quote dot_file);;
if Sys.command com <> 0 then failwith "Exec error";;
